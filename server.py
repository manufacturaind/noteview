import bottle
from bottle import get, run, request, static_file, route, response, hook, auth_basic
try:
    from zklint import Note, get_all_notes
except ModuleNotFoundError:
    from myapp.zklint import Note, get_all_notes
import os
import click
import json
import magic
import markdown
import hashlib
import re
from pathlib import Path

import socket
hostname = socket.gethostname()

if 'opalstack' in hostname:
    BASE_PATH = os.path.join(os.getcwd(), "myapp/content/")
    FRONTEND_PATH = "myapp/frontend/"
else:
    BASE_PATH = os.path.join(os.getcwd(), "content/")
    FRONTEND_PATH = "frontend/"

md = markdown.Markdown(extensions=['meta', 'wikilinks', 'fenced_code', 'tables', 'nl2br'])

app = application = bottle.Bottle()

def stringhash12(s):
    # create a md5 hash from a string and return its first 12 characters
    return hashlib.md5(s.encode('utf-8')).hexdigest()[:12]




# set up CORS
# https://stackoverflow.com/a/41605919
@app.route('/<:re:.*>', method='OPTIONS')
def enable_cors_generic_route():
    add_cors_headers()


@app.hook('after_request')
def enable_cors_after_request_hook():
    add_cors_headers()


def add_cors_headers():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = \
        'GET, POST, PUT, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = \
        'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


# dumb auth
def check_auth(user, pw):
    if user == 'bureau' and pw == 'cracksy':
        return True
    return False


# Routes #############################

@app.route('/')
@auth_basic(check_auth)
def static_root():
    return static_file("index.html", root=FRONTEND_PATH)


@app.get('/api/notes/')
@auth_basic(check_auth)
def browse():
    output = []
    all_notes = get_all_notes(BASE_PATH)
    for note in all_notes:
        contents = md.convert(open(note.filepath, 'r').read())
        title = md.Meta.get('title', note.name)
        # print(md.Meta)
        info = note.metadata
        info['name'] = note.filepath.name.split('.')[0]
        info['title'] = title[0]
        info['date'] = md.Meta.get('date', [''])[0]
        # info['author'] = md.Meta.get('author', [''])[0]
        # info['sentby'] = md.Meta.get('sentby', [''])[0]
        info['author'] = stringhash12(md.Meta.get('author', [''])[0])
        info['size'] = min(10, len(contents)/100)
        if md.Meta.get('tags'):
            info['tags'] = [tag.strip() for tag in md.Meta.get('tags')[0].split(',')]
        output.append(info)
    return {'notes': output}


@app.get('/api/note/')
@auth_basic(check_auth)
def open_file():
    name = request.query.name
    filepath = ''

    filepath = os.path.join(BASE_PATH, name + ".md")
    if not os.path.exists(filepath):
        return

    contents = md.convert(open(filepath, 'r').read())

    title = md.Meta.get('title')

    context = {'contents': contents,
               'name': name,
               'title': title,
               'date': md.Meta.get('date', [''])[0],
               'author': stringhash12(md.Meta.get('author', [''])[0]),
               # 'sentby': stringhash12(md.Meta.get('sentby', [''])[0]),
               'link': md.Meta.get('link', [''])[0],
               'size': min(10, len(contents)/100),
               'tags': [],
            }
    if md.Meta.get('tags'):
        context['tags'] = [tag.strip() for tag in md.Meta.get('tags')[0].split(',')]
    return context


@app.route('/thumbnails/<path:re:.*>')
def static_thumbs(path):
    filename = THUMBNAIL_DIR + path
    return static_file(filename, root='.')


@app.route('/static/<filepath:path>')
def static_set(filepath):
    return static_file(filepath, root=FRONTEND_PATH)


@app.route('/raw/<filepath:path>')
def static_rawfile(filepath):
    return static_file(filepath, root=BASE_PATH)


# Init ###############################################

@click.command()
def serve():
    if 'opalstack' in hostname:
        run(app=app, host='0.0.0.0', port=27216)
    else:
        run(app=app, host='0.0.0.0', port=8000, reloader=True)

if __name__ == "__main__":
    serve()
