#!/usr/bin/env python3
'''
zklint - Find orphan notes

TODO:
    - create meta pages: alphabetical index, orphans
'''
import pathlib
import re
from pprint import pprint

INPUT_DIR = pathlib.Path('/home/rlafuente/nextcloud/Zettel')
RE_ID_LINE = r'^(\d{14})$'
RE_LINK = r'\[\[(\d{14})\]\]'


class Note:
    def __init__(self, filename):
        # accepts a path object or filename string
        if type(filename) == str:
            self.filepath = INPUT_DIR.joinpath(filename)
        else:
            self.filepath = filename
        self.content = self.filepath.read_text()
        self.id = get_note_id(self.content)

    @property
    def name(self):
        return self.filepath.name.replace('.md', '').strip()

    @property
    def metadata(self):
        return {'name': self.name, 'id': self.id}


def make_index():
    notepaths = get_all_filenames()
    notes = []
    for notepath in notepaths:
        note = Note(notepath)
        notes.append(note)

    # Alphabetical index
    notes.sort(key=lambda note: note.name.lower())
    content = '# index\n'
    content += '0000000000000001\n\n'
    for note in notes:
        content += '- [[{}]] {}\n'.format(note.id, note.name)
    indexpath = INPUT_DIR.joinpath('index-alphabetical.md')
    indexpath.write_text(content)

    # Creation date index (by ID)
    notes.sort(key=lambda note: note.id if note.id else '99999999999999')
    content = '# index\n'
    content += '0000000000000002\n\n'
    for note in notes:
        content += '- [[{}]] {}\n'.format(note.id, note.name)
    indexpath = INPUT_DIR.joinpath('index-created.md')
    indexpath.write_text(content)


def get_note_id(text):
    '''Returns the note ID, obtained from searching the file
    for the RE_ID_LINE regex'''
    result = re.search(RE_ID_LINE, text, re.MULTILINE)
    if result:
        return result.group(1)
    return None


def get_note_from_id(id):
    pass


def get_all_filenames(dirname=INPUT_DIR):
    return pathlib.Path(dirname).glob("*.md")

def get_all_ids_with_filenames(dirname=INPUT_DIR):
    return {get_note_id(open(fn, 'r').read()):fn for fn in get_all_filenames()}

def get_all_ids(dirname=INPUT_DIR):
    return [get_note_id(open(fn, 'r').read()) for fn in get_all_filenames()]

def get_all_notes(dirname=INPUT_DIR):
    return [Note(fn) for fn in get_all_filenames(dirname)]

def get_notes_without_id(shortname=False):
    notes = []
    for fn in get_all_filenames():
        text = open(fn, 'r').read()
        note_id = get_note_id(text)
        if not note_id:
            notes.append(fn)
    if shortname:
        notes = [notepath.name for notepath in notes]
    return notes


def get_orphan_notes(shortname=False):
    '''List of paths for each note without any backlinks to it.

    With `shortname` set to True, the list items will be the note
    names (filenames without the extension), otherwise they'll be
    Path objects from the pathlib module
    '''
    orphans = []
    all_links = []
    idmap = get_all_ids_with_filenames()
    for fn in get_all_filenames():
        if fn.name.startswith('index-'):
            # every note has a backlink in the indexes, so ignore it
            continue
        text = open(fn, 'r').read()
        links = re.findall(RE_LINK, text, re.MULTILINE)
        all_links.extend(links)
    for id in get_all_ids():
        if id not in all_links:
            orphans.append(idmap[id])
    if shortname:
        orphans = [orphanpath.name for orphanpath in orphans]
    return orphans


if __name__ == '__main__':
    from pprint import pprint

    '''
    print("Notes without ID:")
    pprint(get_notes_without_id(shortname=True))
    print()
    print("Orphan notes:")
    pprint(get_orphan_notes(shortname=True))
    '''
    make_index()
